package main

import (
	"testing"
)

func TestGetExcelColumnName(t *testing.T) {
	type args struct {
		columnNumber int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Test1", args{columnNumber: 1}, "A"},
		{"Test2", args{columnNumber: 26}, "Z"},
		{"Test3", args{columnNumber: 27}, "AA"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := column2name(tt.args.columnNumber); got != tt.want {
				t.Errorf("GetExcelColumnName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_name2column(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{"Test1", args{name: "A"}, 1},
		{"Test2", args{name: "Z"}, 26},
		{"Test3", args{name: "AA"}, 27},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := name2column(tt.args.name); got != tt.want {
				t.Errorf("name2column() = %v, want %v", got, tt.want)
			}
		})
	}
}
