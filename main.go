package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/tealeg/xlsx"
)

type SourceSheet struct {
	AliasName       string
	FileName        string
	SheetName       string
	SheetIndex      int
	SheetData       *xlsx.Sheet
	SheetHeader     []string
	SortFlag        bool
	KeyColumns      []int
	KeyNames        []string
	DataStart       int
	MultiLineHeader bool
	AliasPrefix     bool
	SkipKeys        bool
}

type stringSlice []string

type AppConfig struct {
	ConfigFilename string
	Configuration  []string
	SilentMode     bool
	HideKeyString  bool
	SourceFiles    map[string]*SourceSheet
	OutputBuffer   map[string]stringSlice
	OutputHeader   stringSlice
}

func column2name(columnNumber int) string {
	columnName := ""
	for columnNumber > 0 {
		modulo := (columnNumber - 1) % 26
		columnName = string(rune(65+modulo)) + columnName
		columnNumber = (columnNumber - modulo) / 26
	}

	return columnName
}

func name2column(name string) int {
	number := 0
	pow := 1
	for i := len(name) - 1; i >= 0; i-- {
		number += (int(rune(name[i])) - 65 + 1) * pow
		pow *= 26
	}

	return number
}

func (app *AppConfig) parseSection(startIndex int) (*SourceSheet, error) {
	var err error
	src := &SourceSheet{
		AliasName:       "",
		FileName:        "",
		SheetName:       "",
		SheetIndex:      0,
		SheetData:       nil,
		SheetHeader:     []string{},
		SortFlag:        false,
		KeyColumns:      []int{},
		DataStart:       0,
		MultiLineHeader: false,
		AliasPrefix:     true,
		SkipKeys:        false,
	}
	src.AliasName = app.Configuration[startIndex]
	src.AliasName = src.AliasName[1 : len(src.AliasName)-1]
	for _, rawLine := range app.Configuration[startIndex+1:] {
		if strings.TrimSpace(rawLine) == "" {
			continue
		} else if strings.HasPrefix(rawLine, "[") && strings.HasSuffix(rawLine, "]") {
			return src, err
		}
		parts := strings.Split(rawLine, "=")
		if len(parts) < 2 {
			return nil, errors.New("format_error")
		}
		key := strings.TrimSpace(strings.ToLower(parts[0]))
		parts[1] = strings.TrimSpace(parts[1])
		switch key {
		case "multilineheader":
			src.MultiLineHeader, err = strconv.ParseBool(strings.TrimSpace(parts[1]))
			if err != nil {
				return nil, fmt.Errorf("not a valid boolean value (Section %s, Line %s)", src.AliasName, parts[1])
			}
		case "aliasprefix":
			src.AliasPrefix, err = strconv.ParseBool(strings.TrimSpace(parts[1]))
			if err != nil {
				return nil, fmt.Errorf("not a valid boolean value (Section %s, Line %s)", src.AliasName, parts[1])
			}
		case "skipkeys":
			src.SkipKeys, err = strconv.ParseBool(strings.TrimSpace(parts[1]))
			if err != nil {
				return nil, fmt.Errorf("not a valid boolean value (Section %s, Line %s)", src.AliasName, parts[1])
			}
		case "filename":
			src.FileName = parts[1]
		case "sheetname":
			src.SheetName = parts[1]
		case "sheetindex":
			src.SheetIndex, err = strconv.Atoi(parts[1])
			if err != nil {
				return nil, fmt.Errorf("%s is not a valid integer (Section %s, Line %s)", parts[1], src.AliasName, rawLine)
			}
		case "datastart":
			src.DataStart, err = strconv.Atoi(parts[1])
			if err != nil {
				return nil, fmt.Errorf("%s is not a valid integer (Section %s, Line %s)", parts[1], src.AliasName, rawLine)
			}
			src.DataStart = src.DataStart - 1 // convert from one-based index inconfig to zero-based here
		case "keycolumns":
			result := []int{}
			names := strings.Split(parts[1], ",")
			for index := range names {
				names[index] = strings.ToUpper(strings.TrimSpace(names[index]))
				result = append(result, name2column(names[index])-1) // convert from one-based in config to zero-based here
			}
			src.KeyColumns = result
			src.KeyNames = names
		case "keynames":
			names := strings.Split(parts[1], ",")
			for index := range names {
				names[index] = strings.TrimSpace(names[index])

			}
			src.KeyNames = names
		case "header":
			names := strings.Split(parts[1], ",")
			for index := range names {
				names[index] = strings.TrimSpace(names[index])

			}
			src.SheetHeader = names
		default:
			return nil, fmt.Errorf("invalid key %s (Section %s, Line %s)", key, src.AliasName, rawLine)
		}
	}
	return src, err
}

func sectionListFromConfig(config []string) []string {
	result := []string{}
	for _, line := range config {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			line = strings.TrimPrefix(line, "[")
			line = strings.TrimSuffix(line, "]")
			result = append(result, strings.TrimSpace(line))
		}
	}
	return result
}

func (app *AppConfig) getSection(sectionName string) map[string]string {
	result := map[string]string{}
	section := "[" + sectionName + "]"
	collect := false
	for _, line := range app.Configuration {
		if strings.EqualFold(strings.TrimSpace(line), section) {
			collect = true
			continue
		} else if strings.HasPrefix(strings.TrimSpace(line), "[") {
			// new section, we're done
			collect = false
			continue
		}
		if collect {
			line = strings.TrimSpace(line)
			if line != "" && !strings.HasPrefix(line, "#") {
				key := strings.ToLower(strings.TrimSpace(strings.Split(line, "=")[0]))
				value := strings.TrimSpace(line[strings.Index(line, "=")+1:])
				result[key] = value
			}
		}
	}
	return result
}

func (app *AppConfig) scanSections() error {
	for index, line := range app.Configuration {
		line = strings.ToLower(line)
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") && line != "[output]" && len(app.Configuration) > index+1 {
			newSheet, err := app.parseSection(index)
			if err != nil {
				fmt.Println("Error processing configuration:", err.Error())
				os.Exit(1)
			}
			app.SourceFiles[newSheet.AliasName] = newSheet
		}
	}
	return nil
}

func (app *AppConfig) readConfig(fname string) error {
	var err error
	app.Configuration, err = linesFromConfigFile(fname)
	if err != nil {
		return err
	}
	err = app.scanSections()
	if err != nil {
		return err
	}
	cfg := app.getSection("output")
	tmp, ok := cfg["hidekeystring"]
	app.HideKeyString, err = strconv.ParseBool(strings.TrimSpace(tmp))
	if !ok {
		app.HideKeyString = false
	}
	return err
}

func trimComment(line string) string {
	index := strings.Index(line, "#")
	if index >= 0 {
		line = strings.TrimSuffix(line, line[index:])
	}
	return strings.TrimSpace(line)
}

func linesFromConfigFile(fname string) ([]string, error) {
	// read lines from file
	file, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var txtlines []string
	for scanner.Scan() {
		txtlines = append(txtlines, scanner.Text())
	}
	// remove comments
	for index, line := range txtlines {
		line = trimComment(line)
		txtlines[index] = strings.TrimSpace(line)
	}
	return txtlines, nil
}

func Preflight() *AppConfig {
	app := &AppConfig{}
	app.SourceFiles = make(map[string]*SourceSheet)
	app.OutputBuffer = make(map[string]stringSlice)
	app.OutputHeader = []string{}

	flag.StringVar(&app.ConfigFilename, "config", "./xlsxjoin.cfg", "filename of the configuration file")
	flag.BoolVar(&app.SilentMode, "silent", false, "silent mode, do not show progress messages")
	flag.Parse()

	return app
}

func (app *AppConfig) loadSourceData() error {
	var err error
	for aliasName, source := range app.SourceFiles {
		wb, err := xlsx.OpenFile(source.FileName)
		if err != nil {
			return err
		}
		sheetList := map[string]int{}
		for index, sheet := range wb.Sheets {
			sheetList[sheet.Name] = index + 1
		}
		if source.SheetName != "" {
			// we have a name, but no index
			var ok bool
			source.SheetData, ok = wb.Sheet[source.SheetName]
			if !ok {
				return fmt.Errorf("no sheet named \"%s\"", source.SheetName)
			}
			source.SheetIndex = sheetList[source.SheetData.Name] // one-based index!
		} else {
			// IMPORTANT! sheet index is one-based to make it easier for end users to specify index values
			// no name, but index. if there is a name *and* an index, the name wins.
			if source.SheetIndex < 1 || source.SheetIndex > len(wb.Sheets) {
				return fmt.Errorf("invalid sheet index %d for alias %s", source.SheetIndex, aliasName)
			}
			source.SheetData = wb.Sheets[source.SheetIndex-1] // one-based index!
			source.SheetName = source.SheetData.Name
		}
		// get the header line, if not already defined in the config file
		if len(source.SheetHeader) == 0 {
			row := source.SheetData.Row(0)
			for _, cell := range row.Cells {
				source.SheetHeader = append(source.SheetHeader, cell.String())
			}
		}
		// get the column index values, if we only have names
		if len(source.KeyColumns) == 0 {
			// check all key column names if the are in the header row
			// if everything works out OK, len(KeyColumns) == len(KeyNames)
			for _, keycol := range source.KeyNames {
				for index, colName := range source.SheetHeader {
					if strings.EqualFold(keycol, colName) {
						source.KeyColumns = append(source.KeyColumns, index)
					}
				}
			}
			if len(source.KeyColumns) != len(source.KeyNames) {
				// we did not find all columns
				return fmt.Errorf("key column name definition for alias %s contains invalid name", aliasName)
			}
		} else {
			// we have keyColumns, so generate names
			source.KeyNames = []string{}
			for _, col := range source.KeyColumns {
				source.KeyNames = append(source.KeyNames, "Spalte "+column2name(col+1))
			}
		}
	}
	return err
}

func (app *AppConfig) getKeyString(aliasName string, dataRow *xlsx.Row) string {
	res := []string{}
	for _, keyColIndex := range app.SourceFiles[aliasName].KeyColumns {
		res = append(res, strings.TrimSpace(dataRow.Cells[keyColIndex].String()))
	}
	return strings.Join(res, " ")
}

func (app *AppConfig) updateHeaderSlice(srcdef *SourceSheet) {
	for index, hdr := range srcdef.SheetHeader {
		skip := false
		for _, keycol := range srcdef.KeyColumns {
			if srcdef.SkipKeys && keycol == index {
				// skip this column
				skip = true
			}
		}
		if skip {
			continue
		}
		if srcdef.AliasPrefix {
			if srcdef.MultiLineHeader {
				hdr = srcdef.AliasName + string(rune(10)) + hdr
			} else {
				hdr = srcdef.AliasName + ":" + hdr
			}
		}
		app.OutputHeader = append(app.OutputHeader, hdr)
	}
}

func (app *AppConfig) processSources() error {
	payloadCount := 0
	if app.HideKeyString == false {
		app.OutputHeader = append(app.OutputHeader, "KEYSTRING")
	}
	aliases := sectionListFromConfig(app.Configuration)
	for _, aliasName := range aliases {
		if strings.ToLower(aliasName) == "output" {
			// skip the output definition
			continue
		}
		if !app.SilentMode {
			fmt.Printf("Processing alias %s\n", aliasName)
		}
		sourceData := app.SourceFiles[aliasName]
		app.updateHeaderSlice(sourceData)
		// loop over all rows, starting with the one defined as "DataStart" in the config
		for index, row := range sourceData.SheetData.Rows[sourceData.DataStart:] {
			key := app.getKeyString(aliasName, row)
			if key == "" {
				return fmt.Errorf("could not find key in row %d for alias %s", index+1, aliasName)
			}
			// check for existing keystring
			_, exists := app.OutputBuffer[key]
			if !exists {
				// create a new entry
				app.OutputBuffer[key] = []string{}
				filler := make([]string, payloadCount)
				app.OutputBuffer[key] = append(app.OutputBuffer[key], filler...)
			}
			// copy the row data
			for index, cell := range row.Cells {
				// check if we have to skip the key columns according to the config file
				skip := false
				for _, keycol := range sourceData.KeyColumns {
					if sourceData.SkipKeys && keycol == index {
						skip = true
					}
				}
				if !skip {
					app.OutputBuffer[key] = append(app.OutputBuffer[key], cell.String())
				}
			}
		}
		payloadCount += len(sourceData.SheetHeader)
	}
	return nil
}

func (app *AppConfig) writeOutput() error {
	cfg := app.getSection("output")
	if len(cfg) == 0 {
		return fmt.Errorf("could not find output configuration (section [Output])")
	}
	outFileName, ok := cfg["filename"]
	if !ok {
		return fmt.Errorf("no output file name defined")
	}
	outSheetName, ok := cfg["sheetname"]
	if !ok {
		outSheetName = "xlsxJoin"
	}
	outf := xlsx.NewFile()
	sh, err := outf.AddSheet(outSheetName)
	if err != nil {
		return err
	}
	row := sh.AddRow()
	for _, hdr := range app.OutputHeader {
		cell := row.AddCell()
		cell.SetString(hdr)
	}
	for key, outRow := range app.OutputBuffer {
		row := sh.AddRow()
		cell := row.AddCell()
		cell.SetString(key)
		if app.HideKeyString {
			outRow = outRow[1:]
		}
		for _, s := range outRow {
			cell = row.AddCell()
			cell.SetString(s)
		}
	}
	if !app.SilentMode {
		fmt.Printf("Writing output to %s\n", outFileName)
	}
	return outf.Save(outFileName)
}

func checkErr(theResult error) {
	if theResult != nil {
		fmt.Println("Runtime error occured, exiting:")
		fmt.Println(theResult.Error())
		os.Exit(1)
	}
}

func main() {
	app := Preflight()
	checkErr(app.readConfig(app.ConfigFilename))
	checkErr(app.loadSourceData())
	checkErr(app.processSources())
	checkErr(app.writeOutput())
	if !app.SilentMode {
		fmt.Println("Done.")
	}
}
