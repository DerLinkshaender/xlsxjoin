
# Kurzanleitung xlsxjoin

Das Programm `xlsxjoin` ist ein Kommandozeilen-basiertes Tool zum Zusammenführen von Daten aus verschiedenen Tabellenblättern in .xlsx-Dateien
über eine definierbare Liste von Spalten, deren Inhalt als Zeichenkette zu einem Vergleichsschlüssel zusammengeführt wird.

## Installation, Voraussetzungen

Das Programm benötigt keine Installation, kopieren Sie die ausführbare Datei für das verwendete Betriebssystem irgendwo hin.
Für den Betrieb wird eine Konfigurationsdatei benötigt, deren Aufbau unten detailliert beschrieben ist.

Das Erstellen der ausführbaren Datei kann entweder selbst aus den Quelldateien erfolgen. Notwendig dazu ist ein installierter go-Compiler
in der Version 1.15 oder höher (Ausführen des Kommandos `go build` im Verzeichnis mit dem Quellcode). Alternativ kann auch eine der
aufgelisteten kompilierten Dateien verwendet werden. Die Nutzung des Quellcoes oder der ausführbaren Dateien erfolgt auf eigenes Risiko 
und ohne jegliche Gewährleistung für irgendeine Eignung für einen bestimmten Verwendungszweck. Zur Prüfung, ob die Datei ohne Veränderung
heruntergeladen wurde, kann die folgende Liste mit SHA256-Prüfsummen verwendet werden.

### Prüfsummen der Binaries

    21d7016a7f5b06138d9cf89d777dc2c08fa9b05ad65a23bf5f3d7c2c7bbf11ad  xlsxjoin-darwin-arm
    52f55ea6e0bac58653537c82ad3e59c1e63a2cb57a4349dbf69c13e3b52c39e0  xlsxjoin-win-amd64.exe
    6073d5c662f6d9e87215223638d6c1d01ea4cc138c16a849bbd23cd8a1c8c4ce  xlsxjoin-darwin-amd64
    c4d773f80eafbad80720d9b00297f052d724d421e609a2babace1f396fd25f1c  xlsxjoin-linux-amd64

## Aufruf

Für Windows:

    xlsxjoin.exe -config DATEINAME  [ -silent ]

Für Mac/Linux:

    xlsxjoin -config DATEINAME  [ -silent ]


Der Parameter `-config` bekommt als Wert den Namen einer Konfigurationsdatei, die dann verwendet wird. Falls diese Option nicht angegeben wird,
wird als Standardwert die Datei `xlsxjoin.cfg` im gleichen Verzeichnis wie das Programm benutzt.

Der Parameter `-silent` verhindert die Ausgabe von Fortschrittsmeldungen und ähnlichen Ausgaben. Er werden nur noch Fehlermeldungen ausgegeben, 
die zum Abbruch des Programms führen.


## Aufbau der Konfigurationsdatei

Die Konfigurationsdatei ist eine Textdatei, die alle Daten für die Ausführung enthält. Das Format entspricht dem bekannten "INI-Format" 
von Windows. Die Datei besteht aus einzelnen Abschnitten, die jeweils durch einen Namen in eckigen Klammern gebildet werden.
In jedem Abschnitt folgenden dann Textzeilen im Format `Schluessel=Wert`. Groß/Kleinschreibung bei den Schlüsselnamen wird ignoriert,
ein `FileNAME` ist das Gleiche wie `fileName`.

Alle leeren Zeilen werden ignoriert und das Zeichen `#` definiert den Beginn eines Kommentars, der bis zum Ende der Zeile gilt (s. Beispiel im nächsten Absatz).


### Definition der Ausgabedatei

Ein Abschnitt muss mit dem Namen `[Output]` in der Datei enthalten sein. Dort finden sich die Daten für die Ausgabedatei.

    # Definition der Ausgabedatei

    [Output]
    Filename=ausgabe.xlsx   # bei Bedarf inkl. Pfad
    SheetName=Ergebnis
    HideKeyString=false  # das ist der Default-Wert

Falls die Angabe von `Filename` fehlt, bricht das Programm mit einer Fehlermeldung ab. Falls die Angabe von `SheetName` fehlt, wird das Tabellenblatt
mit `xlsxJoin` benannt.

Über den Schlüssel `HideKeyString` kann mit einem boolschen Wert definiert werden, ob der KEYSTRING, der für den Vergleich benutzt wird, 
als erste Spalte ausgegeben werden soll oder nicht. Der Defaultwert ist `false`. Wenn bei mindestens einer Eingabedatei der Schlüssel `SkipKeys` auf den 
Wert `false` gesetzt wurde, könnte hier mit `HideKeyString = true` die Ausgabe unterdrückt werden, um mehrfache Ausgaben der Keys zu vermeiden.

### Definition der Eingabedateien

Für jede Eingabedatei wird ein eigener Abschnitt definiert, in dem dann die Eigenschaften der Eingabedatei angegeben werden. Nachfolgend ein Beispiel:

    [Anton]
    Filename=./Mappe1.xlsx
    KeyNames=Vorname, Nachname
    DataStart=2

Der Name des Abschnitts ist der **Aliasname**, der in der Ausgabedatei z.B. dazu verwendet wird, die Spalten zu kennzeichnen. 
Der Aliasname darf keinen Leerraum enthalten und nicht länger sein als 80 Zeichen.

Die Verarbeitung der Eingabedateien erfolgt in der Reihenfolge, in der sie in der Konfigurationsdatei aufgeführt werden. 
Auf diese Weise lässt sich die Reihenfolge der Spalten in der Ausgabedatei keicht dadurch anpassen, dass die Reihenfolge der
Eingabeabschnitte geändert wird.

Für jeden Abschnitt mit einer Eingabedefinition werden folgende Schlüssel unterstützt:

#### AliasPrefix

* Wertebereich: bool-Wert für Ja oder Nein. Folgende Werte werden akzeptiert: 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False
* Bedeutung: Falls ein Wert für logisch Wahr eingetragen wird, dann wird bei den Spalten der Ausgabedatei der Aliasname mit ausgegeben.
* Standardwert: true

Beispiel: im Normalfall enthält die Spaltenbezeichnung der Ausgabedatei den ursprünglichen Spaltennamen und den Aliasnamen, 
z.B. `AntonExport:Nachname`. Falls hier ein Wert für logisch Falsch eingetragen wird, würde nur noch `Nachname` ausgegeben.

#### DataStart

* Wertebereich: ganze Zahl zwischen 1 und max. Anzahl der Zeilen in der Tabelle
* Bedeutung: definiert den Beginn des Datenbereichs (damit die Kopfzeilen ohne Daten übersprungen werden)
* Stadardwert: 1  (bedeutet keine Kopfzeile, nur Daten. Bei einer Zeile Kopf dann 2 eintragen)

#### Filename

* Wertebereich: gültiger Pfad- und Dateiname, z.B. `/tmp/demo/export.xlsx` oder `c:\benutzer\demo\Mappe1.xlsx`
* Standardwert: (keiner)
* Bedeutung: der Name der Eingabedatei

Falls die Datei nicht existiert oder nicht geöffnet werden kann, wird das Programm mit einer Fehlermeldung beendet

#### Header

* Wertebereich: eine kommagetrenne Liste von Spaltennamen für die Kopfzeile, z.B. `VN, NN, User, Pwd`
* Bedeutung: dient dazu, entweder die Bezeichnung der vorhandenen Kopfzeile zu überschreiben oder eine Kopfzeile zu definieren, wenn in der Datei keine vorhanden ist.

#### KeyColumns

* Wertebereich: eine kommagetrennte Liste von Spaltennamen, z.B. `B, A, M` (der Schlüssel zum Vergleichen besteht aus den Spalten B, A und M in dieser Folge)
* Standardwert: (keiner)
* Bedeutung: gibt an, welche Spalten in welcher Reihenfolge die Zeichenkette aufbauen, die zum Vergleich der einzelnen Eingabedateien benutzt wird.

Falls die Daten keine Kopfzeile besitzen, werden die Schlüsselspalten mit diesem Eintrag definiert. Falls es in der Eingabedatei eine Kopfzeile mit Spaltenbezeichnungen
gibt, dann sollte der Schlüssel `KeyNames` verwendet werden.

#### KeyNames

* Wertebereich: eine kommagetrennte Liste von Spaltenbezeichnungen in der Kopfzeile, z.B. `Vorname, Nachname`
* Bedeutung: gibt an, aus welchen Spalten sich die Zeichenkette für den Vergleich der Eingabedateien zusammen setzt.
* Standardwert: (keiner)

#### MultiLineHeader

* Wertebereich: boolscher Wert. Folgende Werte werden akzeptiert: 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False
* Standardwert: false
* Bedeutung: Falls hier ein Wert für logisch Wahr angegegeben wird, werden in den Spalten der Kopfzeile der Aliasname für die Kennzeichnung der Eingabedatei und die Spaltenbezeichnung
  durch eine Zeilenschaltung getrennt. Falls das im erzeugten Excel nicht gleich angezeigt wird, muss nur einmal in einer der Zellen doppelgeklickt werden und dann die Datei 
  gespeichert werden. Das ist ein bekanntes Verhalten von Excel.

#### SheetIndex

* Wertebereich: ganze Zahl von 1 bis zur Anzahl der Tabellenblätter in der Datei
* Bedeutung: gibt an, das wievielte Tabellenblatt in der Eingabedatei gelesen werden soll
* Standardwert: 1 

#### SheetName

* Wertebereich: Name einer existierenden Tabelle in der Eingabedatei
* Bedeutung: gibt an, das welches Tabellenblatt in der Eingabedatei gelesen werden soll
* Standardwert: (keiner) 

Falls sowohl `SheetName` als auch `SheetIndex` angegeben werden, wird  `SheetName` verwendet

#### SkipKeys

* Wertebereich: boolscher Wert. Folgende Werte werden akzeptiert: 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False
* Bedeutung: gibt an, ob die für den Vergleich der Eingabedateien genutzten Spalten in der Ausgabe für diese Eingabedatei unterdrückt werden sollen
* Standardwert: false

Damit nicht aus allen Eingabedateien zum Beispiel immer wieder die Spalten "Vorname" und "Nachname" ausgegeben werden, kann für alle Abschnitte nach dem ersten
der Wert `SkipKeys=true` eingestellt werden und dann werden nur für die erste Eingabedatei die beiden Schlüsselspalten in die Ausgabe übernommen. Das erspart
das manuelle Löschen dieser Spalten nach der Erzeugung der Ausgabedatei.

## Einsatzbeispiel

Nehmen wir an, es gibt drei verschiedene Excel-Tabellen mit Daten von Schülerinnen und Schülern, die in eine Datei vereinheitlicht werden sollen,
damit eine Übersicht über alle Account erstellt werden kann. Der Einfachheit halber gehen wir davon aus, dass jede Datei nur ein Arbeitsblatt
enthält

### Datei 1  (mebis.xlsx)

|Nachname | Vorname | mebis-Login | mebis-Pwd |
|---------|---------|-------------|-----------|
|Meier    | Gaby    | gaby27      | geheim    |
|Testhuber|Franz    | franz28     | pssst     |

### Datei 2 (anton-export.xlsx)

|Vorname  | Nachname| account  | kennwort |
|---------|---------|----------|-----------|
|Franz    |Testhuber|franz28   | pssst     |
|Gaby     |Meier    |gaby27    | geheim    |

### Datei 3 (chat.xlsx)

|firstname| lastname|user      | pwd       |
|---------|---------|----------|-----------|
|Franz    |Testhuber|franz28   | pssst     |
|Martina  |Huber    |martina26 | nixda     |
|Gaby     |Meier    |gaby27    | geheim    |


### Vorgehen

Diese drei Dateien sollen nun in eine Datei zusammengeführt werden. Der Schüssel, mit dem ein Datensatz eindeutig identifiziert werden kann,
ist jeweils die Kombination aus Vorname und Nachname.

    # zuerst definieren wir die ausgabedatei
    [Output]
    Filename=ausgabe.xlsx
    Sheetname=Accountliste

    [Mebis]
    Filename=mebix.xlsx
    DataStart=2  # die erste Zeile ist die Kopfzeile
    KeyNames=Nachname, Vorname
    AliasPrefix=false

    [Anton]
    Filename=anton-export.xlsx
    DataStart=2
    KeyNamed=Nachname, Vorname
    AliasPrefix=false

    [Chat]
    Filename=chat.xlsx
    DataStart=2
    KeyNamed=lastname, firstname
    AliasPrefix=true  # könnte auch entfallen, da der Standardwert bereits "true" ist

Diese Textdatei wird nun unter dem Namen `demo.cfg` abgespeichert. Anschließen kann das Programm `xlsxjoin` mit dieser Datei als Konfiguration
gestartet werden:

    :: Beispiel für Windows (.exe)
    xlsxjoin.exe -config demo.cfg

Das Ergebnis ist dann eine xlsx-Datei mit einem Tabellenblatt `Accountliste` und dem folgenden Inhalt:

|KEYSTRING      |Nachname | Vorname | mebis-Login | mebis-Pwd |Vorname  | Nachname| account  | kennwort  |chat:firstname|chat:lastname|chat:user | chat:pwd  |
|---------------|---------|---------|-------------|-----------|---------|---------|----------|-----------|--------------|-------------|----------|-----------|
|Meier Gaby     |Meier    | Gaby    | gaby27      | geheim    |Gaby     |Meier    |gaby27    | geheim    |Gaby          |Meier        |gaby27    | geheim    |
|Testhuber Franz|Testhuber|Franz    | franz28     | pssst     |Franz    |Testhuber|franz28   | pssst     |Franz         |Testhuber    |franz28   | pssst     |
|Huber Martina  |         |         |             |           |         |         |          |           |Martina       |Huber        |martina26 | nixda     |

Da Martina Huber nur in einer der drei Dateien vorkommt, sind die Spalten für die beiden anderen Dateien natürlich leer. 
Die erste Spalte enthält die Vergleichszeichenkette (*KEYSTRING*), die für die Übereinstimmung genutzt wird und in jeder drei Dateien individuell definiert wurde.

Die letzten vier Spalten zeigen die Verwendung der Option `AliasPrefix`, hier wurde mit der Angabe von `true` die Ausgabe des Aliasnamens (der Name des 
jeweiligen Konfigurationsabschnitts) vor jeder Spalte aktiviert. Weitere Details in der Beschreibung oben.

Einige der Spalten mit den Namensinformationen enthalten redundante Daten und können mit Hilfe der Konfigurationsoptions `SkipKeys` unterdrückt werden.
Hier ist allerdings Vorsicht geboten, denn wie sich am Beispiel von "Huber, Martina" zeigt, ist es nicht egal, welcher Namensinformationen der drei
Eingabedateien ausgeblendet werden, da Martina nur in der dritten Datei vorkommt. Nötigenfalls ist hier eine Aufbereitung der Eingaedateien nötig.

## Lizenz

Das Programm wird unter der MIT-Lizenz bereit gestellt. Nachfolgend finden sie eine deutsche Übersetzung des Lienztextes.

Copyright (c) 2021 Armin Hanisch

Jedem, der eine Kopie dieser Software und der zugehörigen Dokumentationsdateien (die "Software") erhält, wird hiermit kostenlos die Erlaubnis erteilt, 
ohne Einschränkung mit der Software zu handeln, einschließlich und ohne Einschränkung der Rechte zur Nutzung, zum Kopieren, Ändern, Zusammenführen, 
Veröffentlichen, Verteilen, Unterlizenzieren und/oder Verkaufen von Kopien der Software, und Personen, denen die Software zur Verfügung gestellt wird, 
dies unter den folgenden Bedingungen zu gestatten:

Der obige Urheberrechtshinweis und dieser Genehmigungshinweis müssen in allen Kopien oder wesentlichen Teilen der Software enthalten sein.

DIE SOFTWARE WIRD OHNE MÄNGELGEWÄHR UND OHNE JEGLICHE AUSDRÜCKLICHE ODER STILLSCHWEIGENDE GEWÄHRLEISTUNG, EINSCHLIEßLICH, 
ABER NICHT BESCHRÄNKT AUF DIE GEWÄHRLEISTUNG DER MARKTGÄNGIGKEIT, DER EIGNUNG FÜR EINEN BESTIMMTEN ZWECK UND DER NICHTVERLETZUNG VON RECHTEN DRITTER, 
ZUR VERFÜGUNG GESTELLT. DIE AUTOREN ODER URHEBERRECHTSINHABER SIND IN KEINEM FALL HAFTBAR FÜR ANSPRÜCHE, SCHÄDEN ODER ANDERE VERPFLICHTUNGEN, 
OB IN EINER VERTRAGS- ODER HAFTUNGSKLAGE, EINER UNERLAUBTEN HANDLUNG ODER ANDERWEITIG, DIE SICH AUS, AUS ODER IN VERBINDUNG MIT DER SOFTWARE 
ODER DER NUTZUNG ODER ANDEREN GESCHÄFTEN MIT DER SOFTWARE ERGEBEN.
